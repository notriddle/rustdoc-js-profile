use std::{fs, env};
use std::fs::File;
use std::io::Write;
use std::path::Path;
use std::process::Command;
use serde::{Deserialize, Serialize};
use indexmap::IndexMap;

fn main() {
    assert!(Path::new("node_modules/.bin/0x").exists(), "run `npm install`");
    let mut a = std::env::args();
    a.next().expect("Usage: cargo run -- TOOLCHAIN_OLD TOOLCHAIN_NEW");
    let toolchain_old = a.next().expect("Usage: cargo run -- TOOLCHAIN_OLD TOOLCHAIN_NEW");
    let toolchain_new = a.next().expect("Usage: cargo run -- TOOLCHAIN_OLD TOOLCHAIN_NEW");
    let packages = fs::read_to_string("profiler-packages.toml").expect("packages.toml should exist");
    let packages: IndexMap<String, Package> = toml::from_str(&packages).expect("packages.toml should be valid toml");
    let _ = fs::create_dir_all("tmp");
    let _ = fs::create_dir_all("out");
    let mut index = File::create("out/index.html").unwrap();
    env::set_current_dir("tmp").expect("change working directory to tmp");
    index.write_all(br###"<!DOCTYPE html><html><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><title>Unofficial Rustdoc Search Profiling Tool</title>
    <style type="text/css">
    :root{--main-bg-color: #F9F9FA;--main-txt-color: #0c0c0d;--section-border: #CCC;--section-shadow: #fff;--section-shadow-size:-2px;--footer-border-top-size:2px;--footer-border-top: #fff;--footer-box-shadow: #d7d7db;--footer-target-color: #F9F9FA;--footer-target-box-shadow: #0c0c0d;--footer-target-border: #d7b600;--link-visited: #0060df;--link-bg-image: #b1b1b3;--link-focus-shadow: #0a84ff;--link-focus-drop-shadow: rgba(10, 132, 255, 0.3);--visited: #800080;}
    @media(prefers-color-scheme: dark){
        :root {--main-bg-color: #444;--main-txt-color: #e4e4e4;--link-visited: #e39777;--footer-target-color: #282828;--footer-box-shadow: #999;--section-shadow:#000;--section-shadow-size:1px;--footer-border-top:#CCC;--footer-box-shadow:#222;--footer-border-top-size:1px;--visited: #9799ff}
        :link.share, :visited.share {-webkit-filter: invert(100%); filter: invert(100%); }
    }
    body{margin: 40px auto;max-width: 710px;line-height: 1.6;font-size: 18px;color: var(--main-txt-color);background: var(--main-bg-color);padding: 0 10px;font-family: Georgia, "Times New Roman", serif}
    section{border: solid 1px var(--section-border);border-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAABBSURBVBhXY/hPADCAiLdvP+DEcAUMaKC4uPi4sLDIW0IK3lNBQWlp6X8QBgr8QcP/pKSkPzDw8fH/x4V5eHj/AwAIl4WCOETMYgAAAABJRU5ErkJggg==") 2;padding: 0.5em;margin: 1em 0;overflow: hidden;box-shadow: 0 var(--section-shadow-size) 0 0 var(--section-shadow), var(--section-shadow-size) 0 0 0 var(--section-shadow)}
    h1,h2,h3{line-height: 1.2;font-weight: inherit}
    h1,h2,h3,footer,details{font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Helvetica, Arial, sans-serif}
    h3{margin: 0}
    img{max-width: 90%}
    .center{text-align: center}
    .pull-right{float: right;clear:right}
    footer{font-size: 17px;margin-top: 0.5em;padding-top: 0.25em;border-top:solid var(--footer-border-top-size) var(--footer-border-top);box-shadow: 0 -1px 0 0 var(--footer-box-shadow)}
    footer:target,details[open]{background: var(--footer-target-color);margin: -0.25em -0.25em -0.75em;z-index: 1;position: relative;padding: 0.75em;box-shadow: 0.25em 0.25em 0 var(--footer-box-shadow);border: solid 1px var(--footer-target-border)}
    :link,:visited {text-decoration: none;color:var(--link-visited);background-image: linear-gradient(to bottom, transparent 50%, var(--link-bg-image) 50%);background-repeat: repeat-x;background-size: 2px 2px;background-position: 0 95%}
    :link:focus,:visited:focus {box-shadow: 0 0 0 2px var(--link-focus-shadow), 0 0 0 6px var(--link-focus-drop-shadow)}
    :visited {color: var(--visited)}
    :link.share, :visited.share { background: none; float: right }
    details {font-size: 17px}
    details :link, details :visited { 
        display: block;
        padding: 0.5em;
        background-position: 0 0;
    }
    </style></head>
    <body>
    <header><h1><strong>Unofficial</strong> Rustdoc Search Profiling Tool</h1></header>
"###).unwrap();
    for (name, package) in packages.into_iter() {
        println!("{name}");
        let _ = fs::create_dir_all(format!("../out/{name}/new"));
        let _ = fs::create_dir_all(format!("../out/{name}/old"));
        let up_to_root: std::path::PathBuf = if let Some(subdir) = &package.subdir {
            std::path::Path::new(&subdir[..]).components().map(|_| std::path::Component::ParentDir).collect()
        } else {
            std::path::PathBuf::from(".")
        };
        let up_to_root: String = up_to_root.to_string_lossy().to_string();
        if !Path::new(&name).exists() {
            let out = Command::new("git")
                .arg("clone")
                .arg(&package.git_url)
                .arg(&name)
                .output()
                .expect("run git clone");
            if !out.status.success() {
                panic!("stdout = {stdout:?}, stderr = {stderr:?}", stdout = String::from_utf8_lossy(&out.stdout), stderr = String::from_utf8_lossy(&out.stderr));
            }
        }
        env::set_current_dir(&name).expect("change working directory to work");
        let out = Command::new("git")
            .arg("checkout")
            .arg(&package.git_commit)
            .output()
            .expect("run git checkout");
        if !out.status.success() {
            panic!("stdout = {stdout:?}, stderr = {stderr:?}", stdout = String::from_utf8_lossy(&out.stdout), stderr = String::from_utf8_lossy(&out.stderr));
        }
        let out = Command::new("git")
            .arg("reset")
            .arg("--hard")
            .arg(&package.git_commit)
            .output()
            .expect("run git reset --hard");
        if !out.status.success() {
            panic!("stdout = {stdout:?}, stderr = {stderr:?}", stdout = String::from_utf8_lossy(&out.stdout), stderr = String::from_utf8_lossy(&out.stderr));
        }
        for update in &package.updates {
            let out = Command::new("cargo")
                .arg("update")
                .arg("-p")
                .arg(&update)
                .output()
                .expect(&format!("run cargo update -p {update}"));
            if !out.status.success() {
                panic!("stdout = {stdout:?}, stderr = {stderr:?}", stdout = String::from_utf8_lossy(&out.stdout), stderr = String::from_utf8_lossy(&out.stderr));
            }
        }
        {
            let mut cargo_toml = fs::OpenOptions::new().append(true).open("Cargo.toml").unwrap();
            let mut vb = String::new();
            for (k, v) in &package.patch {
                v.serialize(toml::ser::ValueSerializer::new(&mut vb)).unwrap();
                write!(&mut cargo_toml, "\n\n[patch.crates-io]\n{k} = {vb}\n\n").unwrap();
                vb.clear();
            }
        }
        let out = Command::new("rm")
            .arg("-rf")
            .arg("toolchain_old")
            .output()
            .expect("run cargo doc on old toolchain");
        if !out.status.success() {
            panic!("stdout = {stdout:?}, stderr = {stderr:?}", stdout = String::from_utf8_lossy(&out.stdout), stderr = String::from_utf8_lossy(&out.stderr));
        }
        let out = Command::new("rm")
            .arg("-rf")
            .arg("toolchain_new")
            .output()
            .expect("run cargo doc on old toolchain");
        if !out.status.success() {
            panic!("stdout = {stdout:?}, stderr = {stderr:?}", stdout = String::from_utf8_lossy(&out.stdout), stderr = String::from_utf8_lossy(&out.stderr));
        }
        let mut cmd = Command::new("cargo");
        cmd.arg(format!("+{toolchain_old}"))
            .arg("doc")
            .env("RUSTDOCFLAGS", "--cap-lints=allow")
            .arg("--target-dir")
            .arg(format!("{up_to_root}/toolchain_old"));
        if let Some(subdir) = &package.subdir {
            cmd.current_dir(subdir);
        }
        if package.all_features {
            cmd.arg("--all-features");
        }
        if let Some(features) = &package.features {
            cmd.arg("--features").arg(features.join(","));
        }
        let out = cmd.output()
            .expect("run cargo doc on old toolchain");
        if !out.status.success() {
            panic!("stdout = {stdout:?}, stderr = {stderr:?}", stdout = String::from_utf8_lossy(&out.stdout), stderr = String::from_utf8_lossy(&out.stderr));
        }
        let mut cmd = Command::new("cargo");
        cmd.arg(format!("+{toolchain_new}"))
            .arg("doc")
            .env("RUSTDOCFLAGS", "--cap-lints=allow")
            .arg("--target-dir")
            .arg(format!("{up_to_root}/toolchain_new"));
        if let Some(subdir) = &package.subdir {
            cmd.current_dir(subdir);
        }
        if package.all_features {
            cmd.arg("--all-features");
        }
        if let Some(features) = &package.features {
            cmd.arg("--features").arg(features.join(","));
        }
        let out = cmd
            .output()
            .expect("run cargo doc on new toolchain");
        if !out.status.success() {
            panic!("stdout = {stdout:?}, stderr = {stderr:?}", stdout = String::from_utf8_lossy(&out.stdout), stderr = String::from_utf8_lossy(&out.stderr));
        }
        // Run old under CPU profiler 0x
        let mut cmd = Command::new("../../node_modules/.bin/0x");
        cmd
            .arg("-D")
            .arg(format!("../../out/{name}/old/"))
            .arg("--")
            .arg("node")
            .arg("../../src/tester.js")
            .arg("--doc-folder")
            .arg(Path::new("./toolchain_old/doc").canonicalize().unwrap())
            .arg("--crate-name")
            .arg(&name);
        for query in &package.queries {
            cmd.arg("--query")
                .arg(&query);
        }
        let out = cmd
            .output()
            .expect("run cargo doc on old toolchain");
        if out.status.success() {
            File::create(format!("../../out/{name}/old/stdout.txt")).unwrap().write_all(&out.stdout).unwrap();
            File::create(format!("../../out/{name}/old/stderr.txt")).unwrap().write_all(&out.stderr).unwrap();
        } else {
            panic!("stdout = {stdout:?}, stderr = {stderr:?}", stdout = String::from_utf8_lossy(&out.stdout), stderr = String::from_utf8_lossy(&out.stderr));
        }
        // run old under cgmemtime
        let mut cmd = Command::new("../../target/release/cgmemtime");
        cmd
            .arg("--")
            .arg("node")
            .arg("../../src/tester.js")
            .arg("--doc-folder")
            .arg(Path::new("./toolchain_old/doc").canonicalize().unwrap())
            .arg("--crate-name")
            .arg(&name);
        for query in &package.queries {
            cmd.arg("--query")
                .arg(&query);
        }
        let out = cmd
            .output()
            .expect("run cargo doc on old toolchain");
        if out.status.success() {
            File::create(format!("../../out/{name}/old/stdout-cgmemtime.txt")).unwrap().write_all(&out.stdout).unwrap();
            File::create(format!("../../out/{name}/old/stderr-cgmemtime.txt")).unwrap().write_all(&out.stderr).unwrap();
        } else {
            panic!("stdout = {stdout:?}, stderr = {stderr:?}", stdout = String::from_utf8_lossy(&out.stdout), stderr = String::from_utf8_lossy(&out.stderr));
        }
        // run new under CPU profiler 0x
        let mut cmd = Command::new("../../node_modules/.bin/0x");
        cmd
            .arg("-D")
            .arg(format!("../../out/{name}/new/"))
            .arg("--")
            .arg("node")
            .arg("../../src/tester.js")
            .arg("--doc-folder")
            .arg(Path::new("./toolchain_new/doc").canonicalize().unwrap())
            .arg("--crate-name")
            .arg(&name);
        for query in &package.queries {
            cmd.arg("--query")
                .arg(&query);
        }
        let out = cmd
            .output()
            .expect("run cargo doc on new toolchain");
        if out.status.success() {
            File::create(format!("../../out/{name}/new/stdout.txt")).unwrap().write_all(&out.stdout).unwrap();
            File::create(format!("../../out/{name}/new/stderr.txt")).unwrap().write_all(&out.stderr).unwrap();
        } else {
            panic!("stdout = {stdout:?}, stderr = {stderr:?}", stdout = String::from_utf8_lossy(&out.stdout), stderr = String::from_utf8_lossy(&out.stderr));
        }
        // run new under cgmemtime
        let mut cmd = Command::new("../../target/release/cgmemtime");
        cmd
            .arg("--")
            .arg("node")
            .arg("../../src/tester.js")
            .arg("--doc-folder")
            .arg(Path::new("./toolchain_new/doc").canonicalize().unwrap())
            .arg("--crate-name")
            .arg(&name);
        for query in &package.queries {
            cmd.arg("--query")
                .arg(&query);
        }
        let out = cmd
            .output()
            .expect("run cargo doc on new toolchain");
        if out.status.success() {
            File::create(format!("../../out/{name}/new/stdout-cgmemtime.txt")).unwrap().write_all(&out.stdout).unwrap();
            File::create(format!("../../out/{name}/new/stderr-cgmemtime.txt")).unwrap().write_all(&out.stderr).unwrap();
        } else {
            panic!("stdout = {stdout:?}, stderr = {stderr:?}", stdout = String::from_utf8_lossy(&out.stdout), stderr = String::from_utf8_lossy(&out.stderr));
        }
        env::set_current_dir("..").expect("change working directory to work");
        write!(&mut index, r###"<section>
<h2>{name}</h2>
<dl>
<dt>{toolchain_old}</dt>
<dd><a href="{name}/old/stdout.txt">stdout cpu profiler</a>
&bull; <a href="{name}/old/stderr.txt">stderr cpu profiler</a>
&bull; <a href="{name}/old/stdout-cgmemtime.txt">stdout memory/time profiler</a>
&bull; <a href="{name}/old/stderr-cgmemtime.txt">stderr memory/time profiler</a>
&bull; <a href="{name}/old/flamegraph.html">flamegraph</a></dd>
<dt>{toolchain_new}</dt>
<dd><a href="{name}/new/stdout.txt">stdout cpu profiler</a>
&bull; <a href="{name}/new/stderr.txt">stderr cpu profiler</a>
&bull; <a href="{name}/new/stdout-cgmemtime.txt">stdout memory/time profiler</a>
&bull; <a href="{name}/new/stderr-cgmemtime.txt">stderr memory/time profiler</a>
&bull; <a href="{name}/new/flamegraph.html">flamegraph</a></dd>
</dl>
</section>
"###).unwrap();
    }
}

#[derive(Deserialize)]
struct Package {
    git_url: String,
    git_commit: String,
    subdir: Option<String>,
    queries: Vec<String>,
    #[serde(default, alias = "all-features")]
    all_features: bool,
    #[serde(default)]
    features: Option<Vec<String>>,
    #[serde(default)]
    updates: Vec<String>,
    #[serde(default)]
    patch: IndexMap<String, toml::Value>,
}
