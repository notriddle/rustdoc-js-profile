const fs = require("fs");
const path = require("path");

function loadContent(content) {
    const Module = module.constructor;
    const m = new Module();
    m._compile(content, "tmp.js");
    m.exports.ignore_order = content.indexOf("\n// ignore-order\n") !== -1 ||
        content.startsWith("// ignore-order\n");
    m.exports.exact_check = content.indexOf("\n// exact-check\n") !== -1 ||
        content.startsWith("// exact-check\n");
    m.exports.should_fail = content.indexOf("\n// should-fail\n") !== -1 ||
        content.startsWith("// should-fail\n");
    return m.exports;
}

function shouldIgnoreField(fieldName) {
    return fieldName === "query" || fieldName === "correction" ||
        fieldName === "proposeCorrectionFrom" ||
        fieldName === "proposeCorrectionTo";
}

function lookForEntry(entry, data) {
    return data.findIndex(data_entry => {
        let allGood = true;
        for (const key in entry) {
            if (!Object.prototype.hasOwnProperty.call(entry, key)) {
                continue;
            }
            let value = data_entry[key];
            // To make our life easier, if there is a "parent" type, we add it to the path.
            if (key === "path" && data_entry["parent"] !== undefined) {
                if (value.length > 0) {
                    value += "::" + data_entry["parent"]["name"];
                } else {
                    value = data_entry["parent"]["name"];
                }
            }
            if (value !== entry[key]) {
                allGood = false;
                break;
            }
        }
        return allGood === true;
    });
}

// This function checks if `expected` has all the required fields needed for the checks.
function checkNeededFields(fullPath, expected, error_text, queryName, position) {
    let fieldsToCheck;
    if (fullPath.length === 0) {
        fieldsToCheck = [
            "foundElems",
            "original",
            "returned",
            "userQuery",
            "error",
        ];
    } else if (fullPath.endsWith("elems") || fullPath.endsWith("returned")) {
        fieldsToCheck = [
            "name",
            "fullPath",
            "pathWithoutLast",
            "pathLast",
            "generics",
        ];
    } else if (fullPath.endsWith("generics")) {
        fieldsToCheck = [
            "name",
            "fullPath",
            "pathWithoutLast",
            "pathLast",
            "generics",
        ];
    } else {
        fieldsToCheck = [];
    }
    for (const field of fieldsToCheck) {
        if (!Object.prototype.hasOwnProperty.call(expected, field)) {
            let text = `${queryName}==> Mandatory key \`${field}\` is not present`;
            if (fullPath.length > 0) {
                text += ` in field \`${fullPath}\``;
                if (position !== null) {
                    text += ` (position ${position})`;
                }
            }
            error_text.push(text);
        }
    }
}

function valueCheck(fullPath, expected, result, error_text, queryName) {
    if (Array.isArray(expected)) {
        let i;
        for (i = 0; i < expected.length; ++i) {
            checkNeededFields(fullPath, expected[i], error_text, queryName, i);
            if (i >= result.length) {
                error_text.push(`${queryName}==> EXPECTED has extra value in array from field ` +
                    `\`${fullPath}\` (position ${i}): \`${JSON.stringify(expected[i])}\``);
            } else {
                valueCheck(fullPath + "[" + i + "]", expected[i], result[i], error_text, queryName);
            }
        }
        for (; i < result.length; ++i) {
            error_text.push(`${queryName}==> RESULT has extra value in array from field ` +
                `\`${fullPath}\` (position ${i}): \`${JSON.stringify(result[i])}\` ` +
                "compared to EXPECTED");
        }
    } else if (expected !== null && typeof expected !== "undefined" &&
               expected.constructor == Object) { // eslint-disable-line eqeqeq
        for (const key in expected) {
            if (shouldIgnoreField(key)) {
                continue;
            }
            if (!Object.prototype.hasOwnProperty.call(expected, key)) {
                continue;
            }
            if (!Object.prototype.hasOwnProperty.call(result, key)) {
                error_text.push("==> Unknown key \"" + key + "\"");
                break;
            }
            let result_v = result[key];
            if (result_v !== null && key === "error") {
                result_v.forEach((value, index) => {
                    value = value.split("&nbsp;").join(" ");
                    if (index % 2 === 1) {
                        result_v[index] = "`" + value + "`";
                    } else {
                        result_v[index] = value;
                    }
                });
                result_v = result_v.join("");
            }
            const obj_path = fullPath + (fullPath.length > 0 ? "." : "") + key;
            valueCheck(obj_path, expected[key], result_v, error_text, queryName);
        }
    } else {
        const expectedValue = JSON.stringify(expected);
        const resultValue = JSON.stringify(result);
        if (expectedValue !== resultValue) {
            error_text.push(`${queryName}==> Different values for field \`${fullPath}\`:\n` +
                `EXPECTED: \`${expectedValue}\`\nRESULT:   \`${resultValue}\``);
        }
    }
}

/**
 * Load searchNNN.js and search-indexNNN.js.
 *
 * @param {string} doc_folder      - Path to a folder generated by running rustdoc
 * @param {string} resource_suffix - Version number between filename and .js, e.g. "1.59.0"
 * @returns {Object}               - Object containing keys: `doSearch`, which runs a search
 *   with the loaded index and returns a table of results; `parseQuery`, which is the
 *   `parseQuery` function exported from the search module; and `getCorrections`, which runs
 *   a search but returns type name corrections instead of results.
 */
function loadSearchJS(doc_folder, resource_suffix) {
    const searchIndexJs = path.join(doc_folder, "search-index" + resource_suffix + ".js");
    const searchIndex = require(searchIndexJs);

    globalThis.searchState = {
        descShards: new Map(),
        loadDesc: async function({descShard, descIndex}) {
            if (descShard.promise === null) {
                descShard.promise = new Promise((resolve, reject) => {
                    descShard.resolve = resolve;
                    const ds = descShard;
                    const fname = `${ds.crate}-desc-${ds.shard}-${resource_suffix}.js`;
                    fs.readFile(
                        `${doc_folder}/search.desc/${descShard.crate}/${fname}`,
                        (err, data) => {
                            if (err) {
                                reject(err);
                            } else {
                                eval(data.toString("utf8"));
                            }
                        },
                    );
                });
            }
            const list = await descShard.promise;
            return list[descIndex];
        },
        loadedDescShard: function(crate, shard, data) {
            //console.log(this.descShards);
            this.descShards.get(crate)[shard].resolve(data.split("\n"));
        },
        paramNameShards: new Map(),
        paramNameResolvers: new Map(),
        loadParamNames: async function(crate) {
            if (this.paramNameShards.has(crate)) {
                return this.paramNameShards.get(crate);
            } else {
                const promise = new Promise((resolve, reject) => {
                    this.paramNameResolvers.set(crate, resolve);
                    const fname = `${crate}-param-names${resource_suffix}.js`;
                    fs.readFile(
                        `${doc_folder}/search.desc/${crate}/${fname}`,
                        (err, data) => {
                            if (err) {
                                reject(err);
                            } else {
                                eval(data.toString("utf8"));
                            }
                        },
                    );
                });
                this.paramNameShards.set(crate, promise);
                return promise;
            }
        },
        loadedParamNames: function(crate, data) {
            this.paramNameResolvers.get(crate)(JSON.parse(data));
        },
    };


    const staticFiles = path.join(doc_folder, "static.files");
    const searchJs = fs.readdirSync(staticFiles).find(f => f.match(/search.*\.js$/));
    const searchModule = require(path.join(staticFiles, searchJs));
    const searchWords = searchModule.initSearch(searchIndex.searchIndex);

    return {
        doSearch: function(queryStr, filterCrate, currentCrate) {
            const query = searchModule.parseQuery(queryStr);
            if (searchWords) {
                return searchModule.execQuery(query, searchWords, filterCrate, currentCrate);
            } else {
                return searchModule.execQuery(query, filterCrate, currentCrate);
            }
        },
        getCorrections: function(queryStr, filterCrate, currentCrate) {
            const parsedQuery = searchModule.parseQuery(queryStr);
            if (searchWords) {
                searchModule.execQuery(parsedQuery, searchWords, filterCrate, currentCrate);
            } else {
                searchModule.execQuery(parsedQuery, filterCrate, currentCrate);
            }
            return parsedQuery.correction;
        },
        parseQuery: searchModule.parseQuery,
    };
}

function showHelp() {
    console.log("rustdoc-js options:");
    console.log("  --doc-folder [PATH]        : location of the generated doc folder");
    console.log("  --help                     : show this message then quit");
    console.log("  --crate-name [STRING]      : crate name to be used");
    console.log("  --query [QUERY]            : searches to run");
}

function parseOptions(args) {
    const opts = {
        "crate_name": "",
        "doc_folder": "",
        "resource_suffix": "",
        "queries": [],
    };
    const correspondences = {
        "--doc-folder": "doc_folder",
        "--query": "queries",
        "--crate-name": "crate_name",
    };

    for (let i = 0; i < args.length; ++i) {
        const arg = args[i];
        if (Object.prototype.hasOwnProperty.call(correspondences, arg)) {
            i += 1;
            if (i >= args.length) {
                console.log("Missing argument after `" + arg + "` option.");
                return null;
            }
            const arg_value = args[i];
            if (arg !== "--query") {
                opts[correspondences[arg]] = arg_value;
            } else {
                opts[correspondences[arg]].push(arg_value);
            }
        } else if (arg === "--help") {
            showHelp();
            process.exit(0);
        } else {
            console.log("Unknown option `" + arg + "`.");
            console.log("Use `--help` to see the list of options");
            return null;
        }
    }
    if (opts["doc_folder"].length < 1) {
        console.log("Missing `--doc-folder` option.");
    } else if (opts["crate_name"].length < 1) {
        console.log("Missing `--crate-name` option.");
    } else if (opts["queries"].length < 1) {
        console.log("At least one `--query` option is required.");
    } else {
        return opts;
    }
    return null;
}

async function main(argv) {
    const opts = parseOptions(argv.slice(2));
    if (opts === null) {
        return 1;
    }

    const parseAndSearch = loadSearchJS(
        opts["doc_folder"],
        opts["resource_suffix"]
    );

    if (opts["queries"].length !== 0) {
        for (const query of opts["queries"]) {
            process.stdout.write(`Testing ${query} ... `);
            const start = new Date();
            const result = await parseAndSearch.doSearch(query, null, opts["crate_name"]);
            const end = new Date();
            process.stdout.write(`in_args = ${result.in_args.length}, returned = ${result.returned.length}, others = ${result.others.length}`);
            process.stdout.write("\n");
            process.stdout.write(`wall time = ${end.valueOf() - start.valueOf()}`)
            process.stdout.write("\n\n");
        }
    }
    return 0;
}

main(process.argv).catch(e => {
    console.log(e);
    process.exit(1);
}).then(x => process.exit(x));

process.on("beforeExit", () => {
    console.log("process did not complete");
    process.exit(1);
});

