// almost line-by-line conversion of
// https://github.com/gsauthof/cgmemtime/blob/ef9713d0fc4aed7977a4645473c351976135aa3e/cgmemtime.c
//
// cgmemtime - high-water memory usage of a command including descendant processes
//
// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: © 2022 Georg Sauthoff <mail@gms.tf>
// SPDX-FileCopyrightText: © 2024 Michael Howell <michael@notriddle.com>

use std::ffi::{CStr, OsStr};
use std::fmt::Display;

use std::os::unix::ffi::{OsStringExt, OsStrExt};
use std::{mem::size_of, io, io::{Write, Read}, ffi::{CString, OsString}, path::{Path, PathBuf}, fs::File};

use libc::{c_char, c_int, c_long, c_ulonglong, c_void, syscall};

use memchr::memmem;

use syscall_numbers::native::*;

const CLONE_INTO_CGROUP: c_ulonglong = 0x200000000;
const CLONE_PIDFD: c_ulonglong = 0x00001000;
const CLONE_VFORK: c_ulonglong = 0x00004000;

#[repr(C)]
#[allow(nonstandard_style)]
struct clone_args {
    flags: u64,
    pidfd: u64,
    child_tid: u64,
    parent_tid: u64,
    exit_signal: u64,
    stack: u64,
    stack_size: u64,
    tls: u64,
    set_tid: u64,
    set_tid_size: u64,
    cgroup: u64,
}

unsafe fn clone3(args: &mut clone_args) -> c_int {
    let r: c_long = syscall(SYS_clone3, args as *mut clone_args, size_of::<clone_args>());
    r.try_into().expect("clone3 returns an int, so it should fit")
}

unsafe fn raw_waitid(idtype: libc::idtype_t, id: libc::id_t, infop: *const libc::siginfo_t, options: c_int, rusage: *mut libc::rusage) -> c_int {
    let r: c_long = syscall(SYS_waitid, idtype, id, infop, options, rusage);
    r.try_into().expect("waitid returns `0`, or a negative errno")
}

struct Args {
    cg_fs_dir: PathBuf,
    cg_dir: PathBuf,
    child_argv: Vec<CString>,
    machine_readable: bool,
    delim: c_char,
    no_systemd_run: bool,
}

fn help(mut w: impl Write, argv0: &str) -> io::Result<()> {
    write!(w, r##"{argv0} - high-water memory usage of a command including descendants
Usage: {argv0} [OPTION..] COMMAND [COMMAND_OPTION..]

Options:
    -h            Display this help text
    -m CFGS       Cgroup v2 base (default /sys/fs/cgroup)
    -c BASE_DIR   Cgroup directory, must end in XXXXXX
                  (default: $CGFS/user.slice/user-$UID.slice/user@$UID.service/cgmt-$RANDOM)
    -t            machine readable output (delimited columns)
    -d CHAR       column delimiter (default `;`)
    -Z            disable falling back to systemd-run

2022, Georg Sauthoff, GPLv3+
2024, Michael Howell, GPLv3+
"##)
}

fn parse_args<'a>(input: &'a [OsString]) -> Result<Args, Box<dyn std::error::Error>> {
    let mut args = Args {
        cg_fs_dir: PathBuf::from("/sys/fs/cgroup"),
        cg_dir: PathBuf::from("/"),
        child_argv: Vec::new(),
        machine_readable: false,
        delim: b';' as c_char,
        no_systemd_run: false,
    };
    let mut parser = lexopt::Parser::from_args(&input[1..]);
    while let Some(arg) = parser.next()? {
        match arg {
            lexopt::Arg::Value(value) => {
                let mut child_argv = vec![value];
                if let Ok(values) = parser.values() {
                    child_argv.extend(values);
                }
                args.child_argv = child_argv.into_iter().map(|os_string| {
                    let mut c_string = os_string.into_vec();
                    c_string.push(0);
                    CString::from_vec_with_nul(c_string).expect("I just pushed a NUL")
                }).collect();
            }
            lexopt::Arg::Short('m') => {
                args.cg_fs_dir = PathBuf::from(parser.value()?);
            }
            lexopt::Arg::Short('c') => {
                args.cg_dir = PathBuf::from(parser.value()?);
            }
            lexopt::Arg::Short('d') => {
                args.delim = match parser.value()?.as_bytes() {
                    &[c] => c as c_char,
                    bytes => {
                        return Err(format!("Expected 1 byte, found {} bytes", bytes.len()).into());
                    }
                };
            }
            lexopt::Arg::Short('h') | lexopt::Arg::Long("help") => {
                help(std::io::stdout(), &input[0].to_string_lossy())?;
                std::process::exit(0);
            }
            lexopt::Arg::Short('t') => {
                args.machine_readable = true;
            }
            lexopt::Arg::Short('Z') => {
                args.no_systemd_run = true;
            }
            unexpected => return Err(unexpected.unexpected().into()),
        }
    }
    Ok(args)
}

#[derive(Debug)]
enum CurrentCgroupError {
    OpenProcSelfCgroup(io::Error),
    ReadProcSelfCgroup(io::Error),
    EmptyProcSelfCgroup,
    CgroupDoesNotContainSlash(CString),
}

impl std::error::Error for CurrentCgroupError {}

impl std::fmt::Display for CurrentCgroupError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::OpenProcSelfCgroup(error) => write!(f, "Can't open /proc/self/cgroup: {error}"),
            Self::ReadProcSelfCgroup(error) => write!(f, "Can't read /proc/self/cgroup: {error}"),
            Self::EmptyProcSelfCgroup => write!(f, "Read nothing from /proc/self/cgroup"),
            Self::CgroupDoesNotContainSlash(cgroup) => write!(f, "Cgroup {} doesn't contain a slash", cgroup.to_string_lossy()),
        }
    }
}

fn current_cgroup() -> Result<CString, CurrentCgroupError> {
    let mut f = File::open("/proc/self/cgroup").map_err(CurrentCgroupError::OpenProcSelfCgroup)?;
    let mut s = Vec::new();
    f.read_to_end(&mut s).map_err(CurrentCgroupError::ReadProcSelfCgroup)?;
    if s.is_empty() {
        return Err(CurrentCgroupError::EmptyProcSelfCgroup);
    }
    match s.iter().copied().position(|c| c == b'/') {
        Some(p) => {
            s.rotate_left(p);
            s.truncate(s.len() - p);
            s.push(0);
            Ok(CString::from_vec_with_nul(s).expect("I just pushed a null"))
        }
        None => {
            s.push(0);
            Err(CurrentCgroupError::CgroupDoesNotContainSlash(
                CString::from_vec_with_nul(s).expect("I just pushed a null")
            ))
        }
    }
}

#[derive(Debug)]
enum SearchCgroupDirError {
    CurrentCgroupError(CurrentCgroupError),
    NotServiceCgroup(CString)
}

impl std::error::Error for SearchCgroupDirError {}

impl std::fmt::Display for SearchCgroupDirError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::CurrentCgroupError(error) => write!(f, "Can't get current cgroup: {error}"),
            Self::NotServiceCgroup(cgroup) => write!(f, ".service not found in cgroup: {}", cgroup.to_string_lossy()),
        }
    }
}

fn search_cgroup_dir(cg_fs_dir: &Path) -> Result<CString, SearchCgroupDirError> {
    let current_cgroup = current_cgroup().map_err(SearchCgroupDirError::CurrentCgroupError)?;
    let p = match memmem::find(current_cgroup.as_bytes(), b".service") {
        Some(p) => p,
        None => {
            return Err(SearchCgroupDirError::NotServiceCgroup(current_cgroup));
        }
    };
    let b = p + 8; // length of entire service cgroup path (".service".len() = 8)
    let mut t = Vec::with_capacity(cg_fs_dir.as_os_str().len() + b + 12 + 1);
    t.extend_from_slice(cg_fs_dir.as_os_str().as_bytes());
    t.extend_from_slice(&current_cgroup.as_bytes()[..b]);
    t.extend_from_slice(b"/cgmt-XXXXXX");
    t.push(0);
    Ok(CString::from_vec_with_nul(t).expect("I just pushed a NUL"))
}

#[derive(Debug)]
enum AddMemoryControllerError {
    OpenFn {
        cg_dir: PathBuf,
        fn_: CString,
        errno: c_int,
    },
    WriteFn {
        cg_dir: PathBuf,
        fn_: CString,
        errno: c_int,
    },
    PartialWriteFn {
        cg_dir: PathBuf,
        fn_: CString,
    },
    CloseFn {
        cg_dir: PathBuf,
        fn_: CString,
        errno: c_int,
    },
}

impl std::error::Error for AddMemoryControllerError {}

impl std::fmt::Display for AddMemoryControllerError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::OpenFn { cg_dir, fn_, errno } => write!(
                f,
                "Can't open {}/{} (errno: {} - {})",
                cg_dir.display(),
                fn_.to_string_lossy(),
                errno,
                // SAFETY: errno is pulled from libc when setting up
                unsafe { CStr::from_ptr(libc::strerror(*errno)).to_string_lossy() }
            ),
            Self::WriteFn { cg_dir, fn_, errno } => write!(
                f,
                "Can't write {}/{} (errno: {} - {})",
                cg_dir.display(),
                fn_.to_string_lossy(),
                errno,
                // SAFETY: errno is pulled from libc when setting up
                unsafe { CStr::from_ptr(libc::strerror(*errno)).to_string_lossy() }
            ),
            Self::PartialWriteFn { cg_dir, fn_ } => write!(
                f,
                "Partial write to {}/{}",
                cg_dir.display(),
                fn_.to_string_lossy(),
            ),
            Self::CloseFn { cg_dir, fn_, errno } => write!(
                f,
                "Can't write {}/{} (errno: {} - {})",
                cg_dir.display(),
                fn_.to_string_lossy(),
                errno,
                // SAFETY: errno is pulled from libc when setting up
                unsafe { CStr::from_ptr(libc::strerror(*errno)).to_string_lossy() }
            ),
        }
    }
}

fn add_memory_controller(cg_fd: c_int, args: &Args) -> Result<(), AddMemoryControllerError> {
    let fn_ = CStr::from_bytes_with_nul(b"cgroup.subtree_control\0").expect("the nul is right there");
    // SAFETY: openat returns an error if cg_fd is bad
    // it might invoke UB if fn_ was bad, but it's a valid filename
    let fd = unsafe { libc::openat(cg_fd, fn_.as_ptr(), libc::O_WRONLY) };
    if fd == -1 {
        // SAFETY: __errno_location always returns a valid pointer
        let errno = unsafe { *libc::__errno_location() };
        return Err(AddMemoryControllerError::OpenFn {
            cg_dir: args.cg_dir.clone(),
            fn_: CString::from(fn_),
            errno,
        });
    }
    let s = b"+memory";
    let l = unsafe { libc::write(fd, s.as_ptr() as *const c_void, s.len()) };
    if l == -1 {
        // SAFETY: __errno_location always returns a valid pointer
        let errno = unsafe { *libc::__errno_location() };
        // SAFETY: `fd` was allocated earlier, and we already checked for -1
        let _ = unsafe { libc::close(fd) };
        return Err(AddMemoryControllerError::WriteFn {
            cg_dir: args.cg_dir.clone(),
            fn_: CString::from(fn_),
            errno,
        });
    }
    if l != s.len().try_into().expect("\"+memory\".len() is shorter than either isize or usize") {
        // SAFETY: `fd` was allocated earlier, and we already checked for -1
        let _ = unsafe { libc::close(fd) };
        return Err(AddMemoryControllerError::PartialWriteFn {
            cg_dir: args.cg_dir.clone(),
            fn_: CString::from(fn_),
        });
    }
    unsafe {
        // SAFETY: fd is a file descriptor, allocated above
        if libc::close(fd) == -1 {
            // SAFETY: __errno_location always returns a valid pointer
            let errno = *libc::__errno_location();
            return Err(AddMemoryControllerError::CloseFn {
                cg_dir: args.cg_dir.clone(),
                fn_: CString::from(fn_),
                errno,
            });
        }
    }
    Ok(())
}

#[derive(Debug)]
enum CheckCgroupFsMemoryError {
    Open(PathBuf, io::Error),
    Read(PathBuf, io::Error),
    Missing {
        verb: &'static str,
        hint: &'static str,
        fn_: PathBuf,
    },
}

impl std::error::Error for CheckCgroupFsMemoryError {}

impl std::fmt::Display for CheckCgroupFsMemoryError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Open(path, error) => write!(f, "Can't open {path}: {error}", path = path.display()),
            Self::Read(path, error) => write!(f, "Can't read {path}: {error}", path = path.display()),
            Self::Missing { verb, hint, fn_ } => write!(f, "Cgroup memory controller isn't {verb} in {fn_} ({hint})", fn_ = fn_.display()),
        }
    }
}

fn check_cgroupfs_memory(args: &Args, filename: &str, verb: &'static str, hint: &'static str) -> Result<(), CheckCgroupFsMemoryError> {
    let mut fn_ = args.cg_fs_dir.clone();
    fn_.push(Path::new(filename));
    let fnb = &fn_;
    let mut f = File::open(&fn_).map_err(|e| CheckCgroupFsMemoryError::Open(fnb.clone(), e))?;
    let mut buf = Vec::with_capacity(1024);
    f.read_to_end(&mut buf).map_err(|e| CheckCgroupFsMemoryError::Read(fn_.clone(), e))?;
    let p = match memmem::find(&buf, b"memory") {
        Some(p) => p,
        None => {
            return Err(CheckCgroupFsMemoryError::Missing { verb, fn_, hint });
        }
    };
    if p + 6 <= buf.len() && buf[p + 6] != b' ' {
        return Err(CheckCgroupFsMemoryError::Missing { verb, fn_, hint });
    }
    Ok(())
}

fn check_cgroupfs(args: &Args) -> Result<(), CheckCgroupFsMemoryError> {
    check_cgroupfs_memory(args, "cgroup.controllers", "available", "perhaps disable via kernel parameter?")?;
    check_cgroupfs_memory(args, "cgroup.subtree_control", "enabled", "systemd should enable it, by default")?;
    Ok(())
}

#[derive(Debug)]
enum SetupCgroupError {
    CreateCgroupDir {
        cg_dir: PathBuf,
        errno: c_int,
    },
    OpenCgroupDir {
        cg_dir: PathBuf,
        errno: c_int,
    },
    MkdirCgroupLeafDir {
        cg_dir: PathBuf,
        errno: c_int,
    },
    OpenCgroupLeafDir {
        cg_dir: PathBuf,
        errno: c_int,
    },
    AddMemoryControllerError(AddMemoryControllerError),
}

impl std::error::Error for SetupCgroupError {}

impl std::fmt::Display for SetupCgroupError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::CreateCgroupDir { cg_dir, errno } => write!(
                f,
                "Can't create {} (errno: {} - {})",
                cg_dir.display(),
                errno,
                // SAFETY: errno is pulled from libc when setting up
                unsafe { CStr::from_ptr(libc::strerror(*errno)).to_string_lossy() }
            ),
            Self::OpenCgroupDir { cg_dir, errno } => write!(
                f,
                "Can't open {} (errno: {} - {})",
                cg_dir.display(),
                errno,
                // SAFETY: errno is pulled from libc when setting up
                unsafe { CStr::from_ptr(libc::strerror(*errno)).to_string_lossy() }
            ),
            Self::OpenCgroupLeafDir { cg_dir, errno } => write!(
                f,
                "Can't open {}/leaf (errno: {} - {})",
                cg_dir.display(),
                errno,
                // SAFETY: errno is pulled from libc when setting up
                unsafe { CStr::from_ptr(libc::strerror(*errno)).to_string_lossy() }
            ),
            Self::MkdirCgroupLeafDir { cg_dir, errno } => write!(
                f,
                "Can't create {}/leaf (errno: {} - {})",
                cg_dir.display(),
                errno,
                // SAFETY: errno is pulled from libc when setting up
                unsafe { CStr::from_ptr(libc::strerror(*errno)).to_string_lossy() }
            ),
            Self::AddMemoryControllerError(error) => write!(f, "Can't add memory controller: {error}"),
        }
    }
}

fn setup_cgroup(args: &mut Args) -> Result<(c_int, c_int), SetupCgroupError> {
    let mut ctmp: Vec<u8> = args.cg_dir.as_os_str().as_bytes().into();
    ctmp.push(0);
    let mkdtmpresult = unsafe {
        // SAFETY: ctmp definitely has a NUL terminator
        libc::mkdtemp(ctmp.as_mut_ptr() as *mut c_char)
    };
    if mkdtmpresult == std::ptr::null_mut() {
        // SAFETY: __errno_location always returns a valid pointer
        let errno = unsafe { *libc::__errno_location() };
        return Err(SetupCgroupError::CreateCgroupDir {
            cg_dir: args.cg_dir.clone(),
            errno,
        });
    }
    let fd = unsafe {
        // SAFETY: this must be a valid directory path, or mkdtemp would've failed.
        let fd = libc::open(ctmp.as_ptr() as *const c_char, libc::O_PATH);
        if fd == -1 {
            // SAFETY: __errno_location always returns a valid pointer
            let errno = *libc::__errno_location();
            return Err(SetupCgroupError::OpenCgroupDir {
                cg_dir: args.cg_dir.clone(),
                errno,
            });
        }
        fd
    };
    // Put the cg_dir path back in the form of a Rust Path
    // It's important that we *consume* the variable after dropping its NUL
    // (at this point, access everything with openat and friends)
    assert_eq!(ctmp.pop(), Some(0));
    let cg_dir = OsString::from_vec(ctmp);
    args.cg_dir = PathBuf::from(cg_dir);
    let cg_fd = unsafe {
        let fn_ = b"leaf\0".as_ptr() as *const c_char;
        // otherwise, without the nested setup we can't add a process to the parent cgroup
        // because we also need to write its cgroup.subtree_control file Cgroup v2
        // disallows doing both (yields EBUSY) - cf. https://unix.stackexchange.com/a/713343/1131
        //
        // SAFETY: the above fd isn't -1, and the fn_ definitely has a nul
        if libc::mkdirat(fd, fn_, 0o700) == -1 {
            // SAFETY: __errno_location always returns a valid pointer
            let errno = *libc::__errno_location();
            // SAFETY: fd was already checked for -1 and hasn't been shared yet
            let _ = libc::close(fd);
            return Err(SetupCgroupError::MkdirCgroupLeafDir {
                cg_dir: args.cg_dir.clone(),
                errno,
            });
        }
        add_memory_controller(fd, args).map_err(SetupCgroupError::AddMemoryControllerError)?;
        // SAFETY: add_memory_controller doesn't close the fd that we pass to it
        // and fn_ is a valid filename with a NUL terminator
        let cg_fd = libc::openat(fd, fn_, libc::O_PATH);
        if cg_fd == -1 {
            // SAFETY: __errno_location always returns a valid pointer
            let errno = *libc::__errno_location();
            // SAFETY: fd was already checked for -1 and hasn't been shared yet
            // (add_memory_controller doesn't store it anywhere)
            let _ = libc::close(fd);
            return Err(SetupCgroupError::OpenCgroupLeafDir {
                cg_dir: args.cg_dir.clone(),
                errno,
            });
        }
        cg_fd
    };
    // At this point, fd and cg_fd are shared
    // nothing else should close them
    Ok((fd, cg_fd))
}

struct RunResult {
    child_user: libc::timeval,
    child_sys: libc::timeval,
    child_wall: libc::timespec,

    child_rss_highwater: libc::size_t,
    cg_rss_highwater: libc::size_t,
}

impl Display for RunResult {
    fn fmt(&self, mut f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "\n")?;
        write!(f, "user: ")?;
        write_timeval(&mut f, &self.child_user)?;
        write!(f, "\n")?;
        write!(f, "sys:  ")?;
        write_timeval(&mut f, &self.child_sys)?;
        write!(f, "\n")?;
        write!(f, "wall: ")?;
        write_timespec(&mut f, &self.child_wall)?;
        write!(f, "\n")?;
        write!(f, "child_RSS_high: {:10} KiB", self.child_rss_highwater / 1024)?;
        write!(f, "\n")?;
        write!(f, "group_mem_high: {:10} KiB", self.cg_rss_highwater / 1024)?;
        write!(f, "\n")?;
        Ok(())
    }
}

impl RunResult {
    fn write_result(&self, mut o: impl std::io::Write, args: &Args) -> Result<(), Box<dyn std::error::Error>> {
        if args.machine_readable {
            self.write_m(o, args)?;
        } else {
            write!(o, "{self}")?;
        }
        Ok(())
    }
    fn write_m(&self, mut o: impl std::io::Write, args: &Args) -> std::io::Result<()> {
        write_timeval_m(&mut o, &self.child_user)?;
        o.write(&[args.delim as u8])?;
        write_timeval_m(&mut o, &self.child_sys)?;
        o.write(&[args.delim as u8])?;
        write_timespec_m(&mut o, &self.child_wall)?;
        o.write(&[args.delim as u8])?;
        write!(&mut o, "{}", self.child_rss_highwater / 1024)?;
        o.write(&[args.delim as u8])?;
        write!(&mut o, "{}", self.cg_rss_highwater / 1024)?;
        o.write(b"\n")?;
        Ok(())
    }
}

fn write_timeval(mut o: impl std::fmt::Write, t: &libc::timeval) -> std::fmt::Result {
    write!(
        o,
        "{:07.3} s",
        t.tv_sec as f64 + (t.tv_usec as f64 / (1000.0 * 1000.0))
    )
}

fn write_timeval_m(mut o: impl std::io::Write, t: &libc::timeval) -> std::io::Result<()> {
    write!(
        o,
        "{}",
        t.tv_sec as f64 + (t.tv_usec as f64 / (1000.0 * 1000.0))
    )
}

fn write_timespec(mut o: impl std::fmt::Write, t: &libc::timespec) -> std::fmt::Result {
    write!(
        o,
        "{:07.3} s",
        t.tv_sec as f64 + (t.tv_nsec as f64 / 1000000000.0)
    )
}

fn write_timespec_m(mut o: impl std::io::Write, t: &libc::timespec) -> std::io::Result<()> {
    write!(
        o,
        "{}",
        t.tv_sec as f64 + (t.tv_nsec as f64 / 1000000000.0)
    )
}

fn sub_ts(a: &libc::timespec, b: &libc::timespec) -> libc::timespec {
    let mut r = libc::timespec {
        tv_sec: a.tv_sec - b.tv_sec,
        tv_nsec: a.tv_nsec - b.tv_nsec,
    };
    if r.tv_nsec < 0 {
        r.tv_sec -= 1;
        r.tv_nsec += 1000000000;
    }
    r
}

#[derive(Debug)]
enum ReadCgRssHighError {
    OpenMemoryPeak {
        errno: c_int,
    },
    ReadMemoryPeak {
        errno: c_int,
    },
    CloseMemoryPeak {
        errno: c_int,
    },
}

impl std::error::Error for ReadCgRssHighError {}

impl std::fmt::Display for ReadCgRssHighError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::OpenMemoryPeak { errno } => write!(
                f,
                "Can't open memory.peak (requires Kernel 5.19 or later): {} - {}",
                errno,
                // SAFETY: errno is pulled from libc when setting up
                unsafe { CStr::from_ptr(libc::strerror(*errno)).to_string_lossy() }
            ),
            Self::ReadMemoryPeak { errno } => write!(
                f,
                "Can't read memory.peak: {} - {}",
                errno,
                // SAFETY: errno is pulled from libc when setting up
                unsafe { CStr::from_ptr(libc::strerror(*errno)).to_string_lossy() }
            ),
            Self::CloseMemoryPeak { errno } => write!(
                f,
                "Can't close memory.peak: {} - {}",
                errno,
                // SAFETY: errno is pulled from libc when setting up
                unsafe { CStr::from_ptr(libc::strerror(*errno)).to_string_lossy() }
            ),
        }
    }
}

fn read_cg_rss_high(cg_fd: c_int) -> Result<libc::size_t, ReadCgRssHighError> {
    unsafe {
        // SAFETY: the filename is definitely NUL-terminated
        // cg_fd is called by a function that supplies a valid fd
        let fd = libc::openat(cg_fd, b"memory.peak\0".as_ptr() as *const c_char, libc::O_RDONLY);
        if fd == -1 {
            // SAFETY: __errno_location always returns a valid pointer
            let errno = *libc::__errno_location();
            return Err(ReadCgRssHighError::OpenMemoryPeak {
                errno,
            });
        }
        let mut b: [c_char; 21] = [0; 21];
        let l = libc::read(fd, (&mut b) as *mut c_char as *mut c_void, b.len() - 1);
        if l == -1 {
            // SAFETY: __errno_location always returns a valid pointer
            let errno = *libc::__errno_location();
            // SAFETY: fd was already checked for -1 and hasn't been shared yet
            // (add_memory_controller doesn't store it anywhere)
            let _ = libc::close(fd);
            return Err(ReadCgRssHighError::ReadMemoryPeak {
                errno,
            });
        }
        if libc::close(fd) == -1 {
            // SAFETY: __errno_location always returns a valid pointer
            let errno = *libc::__errno_location();
            return Err(ReadCgRssHighError::CloseMemoryPeak {
                errno,
            });
        }
        Ok(libc::atol(b.as_ptr()).try_into().unwrap())
    }
}


#[derive(Debug)]
enum ExecuteError {
    StartClock {
        errno: c_int,
    },
    Clone3 {
        errno: c_int,
    },
    SigActionInt {
        errno: c_int,
    },
    SigActionQuit {
        errno: c_int,
    },
    WaitId {
        errno: c_int,
    },
    StopClock {
        errno: c_int,
    },
    ReadCgRssHigh(ReadCgRssHighError),
}

impl std::error::Error for ExecuteError {}

impl std::fmt::Display for ExecuteError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::StartClock { errno } => write!(
                f,
                "gettime start failed: {} - {}",
                errno,
                // SAFETY: errno is pulled from libc when setting up
                unsafe { CStr::from_ptr(libc::strerror(*errno)).to_string_lossy() }
            ),
            Self::Clone3 { errno } => write!(
                f,
                "clone3 failed: {} - {}",
                errno,
                // SAFETY: errno is pulled from libc when setting up
                unsafe { CStr::from_ptr(libc::strerror(*errno)).to_string_lossy() }
            ),
            Self::SigActionInt { errno } => write!(
                f,
                "failed to ignore SIGINT: {} - {}",
                errno,
                // SAFETY: errno is pulled from libc when setting up
                unsafe { CStr::from_ptr(libc::strerror(*errno)).to_string_lossy() }
            ),
            Self::SigActionQuit { errno } => write!(
                f,
                "failed to ignore SIGQUIT: {} - {}",
                errno,
                // SAFETY: errno is pulled from libc when setting up
                unsafe { CStr::from_ptr(libc::strerror(*errno)).to_string_lossy() }
            ),
            Self::WaitId { errno } => write!(
                f,
                "waitid failed: {} - {}",
                errno,
                // SAFETY: errno is pulled from libc when setting up
                unsafe { CStr::from_ptr(libc::strerror(*errno)).to_string_lossy() }
            ),
            Self::StopClock { errno } => write!(
                f,
                "gettime end failed: {} - {}",
                errno,
                // SAFETY: errno is pulled from libc when setting up
                unsafe { CStr::from_ptr(libc::strerror(*errno)).to_string_lossy() }
            ),
            Self::ReadCgRssHigh(error) => write!(f, "failed to read cg rss high water mark: {error}"),
        }
    }
}

fn execute(args: &Args, cg_fd: c_int) -> Result<RunResult, ExecuteError> {
    let mut pid_fd: c_int = -1;
    let mut ca = clone_args {
        flags: CLONE_INTO_CGROUP | CLONE_PIDFD | CLONE_VFORK,
        pidfd: (&mut pid_fd as *mut c_int as usize).try_into().expect("userland pointers cannot be negative"),
        exit_signal: libc::SIGCHLD.try_into().expect("SIGCHLD cannot be negative"),
        cgroup: cg_fd.try_into().expect("cg_fd cannot be negative"),
        child_tid: 0,
        parent_tid: 0,
        stack: 0,
        stack_size: 0,
        tls: 0,
        set_tid: 0,
        set_tid_size: 0,
    };
    // Prepare args in the format clone3 wants them
    let mut child_argv = Vec::with_capacity(args.child_argv.len());
    for arg in &args.child_argv {
        child_argv.push(arg.as_ptr());
    }
    if child_argv.is_empty() {
        panic!("need to supply at least the executable name");
    }
    child_argv.push(std::ptr::null());
    // record starting time
    let mut start: libc::timespec = libc::timespec { tv_sec: 0, tv_nsec: 0 };
    unsafe {
        // SAFETY: the pointer is constructed correctly
        if libc::clock_gettime(libc::CLOCK_MONOTONIC, &mut start as *mut libc::timespec) == -1 {
            // SAFETY: __errno_location always returns a valid pointer
            let errno = *libc::__errno_location();
            return Err(ExecuteError::StartClock { errno });
        }
    }
    // SAFETY: ca is constructed above
    // This program does not use threads, so thread safety isn't an issue
    // and malloc can be safely used in the child process
    let pid = unsafe { clone3(&mut ca) };
    if pid == -1 {
        // SAFETY: __errno_location always returns a valid pointer
        let errno = unsafe { *libc::__errno_location() };
        return Err(ExecuteError::Clone3 { errno });
    }
    if pid == 0 {
        // We're in the child
        unsafe {
            // SAFETY: This dereferences pointers that are pulled, above, from CString
            libc::execvp(child_argv[0], child_argv.as_slice().as_ptr());
            let errno = *libc::__errno_location();
            let errdesc = CStr::from_ptr(libc::strerror(errno)).to_string_lossy();
            eprintln!("execvp failed: {errno} - {errdesc}");
            if errno == libc::ENOENT {
                libc::_exit(127);
            }
            std::process::exit(126);
        }
    } else {
        // We're in the parent
        unsafe {
            let sa = libc::sigaction {
                sa_sigaction: libc::SIG_IGN,
                sa_flags: 0,
                sa_mask: std::mem::zeroed(),
                sa_restorer: None,
            };
            if libc::sigaction(libc::SIGINT, &sa, std::ptr::null_mut()) == -1 {
                // SAFETY: __errno_location always returns a valid pointer
                let errno = *libc::__errno_location();
                return Err(ExecuteError::SigActionInt { errno });
            }
            if libc::sigaction(libc::SIGQUIT, &sa, std::ptr::null_mut()) == -1 {
                // SAFETY: __errno_location always returns a valid pointer
                let errno = *libc::__errno_location();
                return Err(ExecuteError::SigActionQuit { errno });
            }
            let mut info: libc::siginfo_t = std::mem::zeroed();
            let mut usg: libc::rusage = std::mem::zeroed();
            // SAFETY: these pointers are just references
            // the function doesn't store them or anything
            if raw_waitid(libc::P_PIDFD, pid_fd.try_into().unwrap(), &mut info as *mut libc::siginfo_t, libc::WEXITED, &mut usg as *mut libc::rusage) == -1 {
                // SAFETY: __errno_location always returns a valid pointer
                let errno = *libc::__errno_location();
                return Err(ExecuteError::WaitId { errno });
            }
            let mut stop: libc::timespec = libc::timespec { tv_sec: 0, tv_nsec: 0 };
            // SAFETY: the pointer is constructed correctly
            if libc::clock_gettime(libc::CLOCK_MONOTONIC, &mut stop as *mut libc::timespec) == -1 {
                // SAFETY: __errno_location always returns a valid pointer
                let errno = *libc::__errno_location();
                return Err(ExecuteError::StopClock { errno });
            }
            Ok(RunResult {
                child_user: usg.ru_utime,
                child_sys: usg.ru_stime,
                child_wall: sub_ts(&stop, &start),
                child_rss_highwater: (usg.ru_maxrss * 1024).try_into().unwrap(),
                cg_rss_highwater: read_cg_rss_high(cg_fd).map_err(ExecuteError::ReadCgRssHigh)?,
            })
        }
    }
}

fn teardown_cgroup(args: &Args, cgp_fd: c_int, cg_fd: c_int) {
    unsafe {
        let _fn_ = "leaf";
        let fnz = b"leaf\0".as_ptr() as *const c_char;
        if libc::close(cg_fd) == -1 {
            panic!("cannot close cgroup after finishing");
        }
        if libc::unlinkat(cgp_fd, fnz, libc::AT_REMOVEDIR) == -1 {
            panic!("cannot unlink cgroup after finishing");
        }
        if libc::close(cgp_fd) == -1 {
            panic!("cannot close cgroup p after finishing");
        }
        std::fs::remove_dir(&args.cg_dir).expect("remove cg dir after finishing");
    }
}

fn reexec_with_systemd_run() {
    let mut oldargs = std::env::args_os();
    let mut newargs = vec![
        OsString::from("--user"),
        OsString::from("--scope"),
        OsString::from("--quiet"),
        oldargs.next().expect("must have at least argv[0]"),
        OsString::from("-Z"),
    ];
    newargs.extend(oldargs);
    std::process::Command::new("systemd-run")
        .args(newargs)
        .stdout(std::process::Stdio::inherit())
        .stderr(std::process::Stdio::inherit())
        .stdin(std::process::Stdio::inherit())
        .output()
        .unwrap();
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<OsString> = std::env::args_os().collect();
    let mut args = parse_args(&args)?;
    check_cgroupfs(&args)?;
    if args.cg_dir == Path::new("/") {
        match search_cgroup_dir(&args.cg_fs_dir) {
            Ok(r) => {
                unsafe {
                    // Linux doesn't care about encoding
                    args.cg_dir = PathBuf::from(OsStr::from_encoded_bytes_unchecked(r.to_bytes()));
                }
            }
            Err(SearchCgroupDirError::NotServiceCgroup(_)) if !args.no_systemd_run => {
                reexec_with_systemd_run();
                return Ok(());
            }
            Err(e) => {
                return Err(e.into());
            }
        }
    }
    let (cgp_fd, cg_fd) = setup_cgroup(&mut args)?;
    let r = execute(&args, cg_fd)?;
    r.write_result(std::io::stderr(), &args)?;
    teardown_cgroup(&args, cgp_fd, cg_fd);
    Ok(())
}