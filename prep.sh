#!/bin/bash
set -ex
OLD=nightly
NEW=dev
cd cgmemtime; cargo build --release --bin cgmemtime; cd ..
cargo run --release --bin rustdoc-js-profiler $OLD $NEW
rm -f out/*/{old,new}/isolate*
rm -f out/*/{old,new}/*.json*
