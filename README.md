**Unofficial** Rustdoc Search Profiling Tool

# Preparation

Before running this, you need to set up a `dev` toolchain that points at the version you want to test:

```console
$ rustup toolchain link dev /home/myuser/develop/rust/build/x86_64-unknown-linux-gnu/stage2/
```

For more information on this, see [the rustup docs].

[the rustup docs]: https://rust-lang.github.io/rustup/concepts/toolchains.html#custom-toolchains

# Usage

```console
$ vi prep.sh
$ bash prep.sh
```

Then open ./out/index.html

Docs for 0x can be found at <https://github.com/davidmarkclements/0x/tree/master/docs>

